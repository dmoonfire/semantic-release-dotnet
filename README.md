semantic-release-dotnet
=======================

`semantic-release-dotnet` is a [semantic-release](https://semantic-release.gitbook.io/semantic-release/) plugin for setting the varions on .NET projects, in specific `.csproj` and `Directory.Build.props`. It has the ability to create property files if missing or updating existing versions in files that do exist. If an existing file does not have a version property, then one will be added to either the first `<PropertyGroup/>` or by creating a new property group if one is not needed.

This only implements the `prepare` function for semantic-release and does not have functionality for uploading NuGet packages to repositories. This follows the [Single Responsibility Principle](https://en.wikipedia.org/wiki/Single-responsibility_principle), see `semantic-release-nuget` for that.

## Installing

Depending on your package manager, you'll need to run one of the commands.

```
npm install semantic-release-dotnet --only=dev
yarn add semantic-release-dotnet -D
```

Typically this is only used as a development dependency.

## Usage

Add the plugin into the `release.config.js` file or in the appropriate section of `package.json`. An example `release.config.js`:

```
module.exports = {
    plugins: [
        "@semantic-release/commit-analyzer",
        "semantic-release-dotnet",
    ],
};
```

Options can be given also:

```
module.exports = {
    plugins: [
        "@semantic-release/commit-analyzer",
        [
            "semantic-release-dotnet",
            {
                paths: ["src/**.csproj"],
            }
        ],
    ],
};
```

## Options

There are only a few options for the command.

### `paths?: string[]`

* Default: `["Directory.Build.props"]`

This is an array of paths that are passed into [glob](https://www.npmjs.com/package/glob) to figure out what files need to be updated. If it is not provided, then a `Directory.Build.props` will be created and updated at the root level of the repository. If a path does not have magic values (so `src/Project A/Project A.csproj` instead of `**/*.csproj`), then the file will be created if missing.

### `indent?: string | number`

* Default: `"  "``

This determines the indent of the files updated. If this is zero (`0`) or blank (`""`), then no indention will be done. All files that match the pattern will be rewritten and indented.

### `debug?: boolean`

* Default: `false`

Turns on debugging messages. This defaults to `false` to avoid information overload.
