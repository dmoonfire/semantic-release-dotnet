# 1.0.0 (2021-08-30)


### Bug Fixes

* updating package information ([0ca3171](https://gitlab.com/dmoonfire/semantic-release-dotnet/commit/0ca3171bb1f261bf89a0e561baa9b0d12ceb8c65))
