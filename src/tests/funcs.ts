import * as path from "path";
import * as fs from "fs";
import * as rimraf from "rimraf";

export const logger = {
    debug: console.debug,
    warn: () => {},
    error: console.error,
};

/**
 * Creates a test directory that is not checked into the build that is used to
 * test the functionality of the plugin.
 */
export function prepareTest(key: string): string {
    // Make sure the top-level build directory already exists.
    const buildPath = path.join(process.cwd(), "build");

    if (!fs.existsSync(buildPath)) {
        fs.mkdirSync(buildPath);
    }

    // Make sure the project directory exists and is empty.
    const testPath = path.join(buildPath, key);

    if (fs.existsSync(testPath)) {
        rimraf.sync(testPath);
    }

    fs.mkdirSync(testPath);

    return testPath;
}
