import * as fs from "fs";
import * as path from "path";
import { update } from "../prepare";
import { prepareTest, logger } from "./funcs";

test("create default file", async () => {
    const dir = prepareTest("create-default-file");
    const resultPath = path.join(dir, "Directory.Build.props");

    await update({ debug: false }, logger, dir, "1.2.3");

    expect(fs.existsSync(resultPath)).toBe(true);
    expect(fs.readFileSync(resultPath).toString()).toContain(
        "<Version>1.2.3</Version>"
    );
});

test("update default file update version", async () => {
    const dir = prepareTest("update-default-file-update-version");
    const resultPath = path.join(dir, "Directory.Build.props");

    fs.writeFileSync(
        resultPath,
        [
            "<Project>",
            "  <PropertyGroup>",
            "    <Version>1.2.3</Version>",
            "  </PropertyGroup>",
            "</Project>",
            "",
        ].join("\n")
    );

    await update({ debug: false }, logger, dir, "4.5.6");

    expect(fs.existsSync(resultPath)).toBe(true);
    expect(fs.readFileSync(resultPath).toString()).toContain(
        "<Version>4.5.6</Version>"
    );
});

test("update default file add property group", async () => {
    const dir = prepareTest("update-defualt-file-add-property-group");
    const resultPath = path.join(dir, "Directory.Build.props");

    fs.writeFileSync(resultPath, ["<Project>", "</Project>", ""].join("\n"));

    await update({ debug: false }, logger, dir, "4.5.6");

    expect(fs.existsSync(resultPath)).toBe(true);
    expect(fs.readFileSync(resultPath).toString()).toContain(
        "<Version>4.5.6</Version>"
    );
});

test("update default files append property group", async () => {
    const dir = prepareTest("update-default-files-append-property-group");
    const resultPath = path.join(dir, "Directory.Build.props");

    fs.writeFileSync(
        resultPath,
        [
            "<Project>",
            "  <PropertyGroup>",
            "    <Other>other</Other>",
            "  </PropertyGroup>",
            "</Project>",
            "",
        ].join("\n")
    );

    await update({ debug: false }, logger, dir, "4.5.6");

    const content = fs.readFileSync(resultPath).toString();

    expect(fs.existsSync(resultPath)).toBe(true);
    expect(content).toContain("<Version>4.5.6</Version>");
    expect(content).toContain("<Other>other</Other>");
});

test("update multiple files one pattern", async () => {
    const dir = prepareTest("update-multiple-files-one-pattern");
    const resultPath1 = path.join(dir, "t1.csproj");
    const resultPath2 = path.join(dir, "t2.csproj");

    fs.writeFileSync(
        resultPath1,
        [
            "<Project>",
            "  <PropertyGroup>",
            "    <Version>1.2.3</Version>",
            "  </PropertyGroup>",
            "</Project>",
            "",
        ].join("\n")
    );
    fs.writeFileSync(
        resultPath2,
        [
            "<Project>",
            "  <PropertyGroup>",
            "    <Version>2.3.4</Version>",
            "  </PropertyGroup>",
            "</Project>",
            "",
        ].join("\n")
    );

    await update(
        { paths: ["**/*.csproj"], debug: false },
        logger,
        dir,
        "4.5.6"
    );

    expect(fs.existsSync(resultPath1)).toBe(true);
    expect(fs.existsSync(resultPath2)).toBe(true);
    expect(fs.readFileSync(resultPath1).toString()).toContain(
        "<Version>4.5.6</Version>"
    );
    expect(fs.readFileSync(resultPath2).toString()).toContain(
        "<Version>4.5.6</Version>"
    );
});

test("update multiple files two patterns", async () => {
    const dir = prepareTest("update-multiple-files-two-patterns");
    const resultPath1 = path.join(dir, "t1.csproj");
    const resultPath2 = path.join(dir, "t2.csproj");

    fs.writeFileSync(
        resultPath1,
        [
            "<Project>",
            "  <PropertyGroup>",
            "    <Version>1.2.3</Version>",
            "  </PropertyGroup>",
            "</Project>",
            "",
        ].join("\n")
    );
    fs.writeFileSync(
        resultPath2,
        [
            "<Project>",
            "  <PropertyGroup>",
            "    <Version>2.3.4</Version>",
            "  </PropertyGroup>",
            "</Project>",
            "",
        ].join("\n")
    );

    await update(
        { paths: ["**/t1.*", "t2.*"], debug: false },
        logger,
        dir,
        "4.5.6"
    );

    expect(fs.existsSync(resultPath1)).toBe(true);
    expect(fs.existsSync(resultPath2)).toBe(true);
    expect(fs.readFileSync(resultPath1).toString()).toContain(
        "<Version>4.5.6</Version>"
    );
    expect(fs.readFileSync(resultPath2).toString()).toContain(
        "<Version>4.5.6</Version>"
    );
});
