import * as fs from "fs";
import { PluginOptions } from "./PluginOptions";
import * as convert from "xml-js";

export async function updateFile(
    options: PluginOptions,
    logger: any,
    path: string,
    version: string
): Promise<void> {
    // Load the file into memory.
    logger.debug("updating file", path);

    const xml = fs.readFileSync(path).toString();
    const js = convert.xml2js(xml) as convert.Element;

    if (!js.elements) {
        throw new Error("Cannot find top-level elements in " + path);
    }

    // Get the root-level object.
    const root = js.elements.filter(
        (x) => x.type === "element" && x.name === "Project"
    )[0];

    if (!root) {
        throw new Error("Cannot find top-level <Project> element in " + path);
    }

    root.elements = root.elements ?? [];

    // Look through all the property groups and see if we already have a
    // version.
    const groups =
        root.elements.filter(
            (x) => x.type === "element" && x.name == "PropertyGroup"
        ) ?? [];
    let foundVersion = false;
    let firstGroup: convert.Element | undefined = undefined;

    for (const group of groups) {
        if (!group.elements) {
            continue;
        }

        firstGroup = firstGroup ?? group;

        const element = group.elements.filter(
            (x) => x.type === "element" && x.name === "Version"
        )[0];

        if (!element) {
            continue;
        }

        element.elements = [{ type: "text", text: version }];
        foundVersion = true;
        break;
    }

    // If we didn't find the file
    if (!foundVersion) {
        if (!firstGroup) {
            firstGroup = {
                type: "element",
                name: "PropertyGroup",
            };

            root.elements.push(firstGroup);
        }

        firstGroup.elements = firstGroup.elements ?? [];

        firstGroup.elements.push({
            type: "element",
            name: "Version",
            elements: [
                {
                    type: "text",
                    text: version,
                },
            ],
        });
    }

    // Write it all out.
    writeJs(options, path, js);
}

export async function createFile(
    options: PluginOptions,
    logger: any,
    path: string,
    version: string
): Promise<void> {
    logger.debug("creating file", path);

    const js: convert.Element = {
        elements: [
            {
                type: "element",
                name: "Project",
                elements: [
                    {
                        type: "element",
                        name: "PropertyGroup",
                        elements: [
                            {
                                type: "element",
                                name: "Version",
                                elements: [
                                    {
                                        type: "text",
                                        text: version,
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
    };

    writeJs(options, path, js);
}

async function writeJs(
    options: PluginOptions,
    path: string,
    js: convert.Element
): Promise<void> {
    const xml = convert.js2xml(js, { spaces: options.indent }) + "\n";

    fs.writeFileSync(path, Buffer.from(xml, "utf-8"));
}
