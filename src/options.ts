import { PluginOptions } from "./PluginOptions";

export function resolveOptions(options: Partial<PluginOptions>): PluginOptions {
    const resolved: PluginOptions = {
        paths: ["./Directory.Build.props"],
        debug: false,
        indent: "  ",
        ...options,
    };

    resolved.indent = resolved.indent === "\t" ? "\t" : resolved.indent;

    return resolved;
}
