export interface PluginOptions {
    /**
     * A list of paths or minimatchs used to identify the files.
     */
    paths: string[];

    /**
     * Set to true for debugging the library.
     */
    debug: boolean;

    /**
     * How to indent XML. Put the prefix or the number of spaces. Use "\t" to
     * put in tabs.
     */
    indent: string | number;
}
