import { Options } from "semantic-release";
import { PluginOptions } from "./PluginOptions";
import * as glob from "glob-promise";
import { createFile, updateFile } from "./files";
import * as path from "path";
import { resolveOptions } from "./options";

export async function update(
    options: Options & Partial<PluginOptions>,
    logger: any,
    directory: string,
    version: string
): Promise<void> {
    const resolved = resolveOptions(options);

    const debug = resolved.debug ? logger.debug : () => {};
    logger.debug = debug;

    debug("options", resolved);

    // Go throught through the individual files for mapping.
    let updated = false;

    for (const pattern of resolved.paths) {
        // Go through and update any existing files.
        let updatedPath = false;

        debug("scanning path", pattern);

        await glob(pattern, { cwd: directory }).then(async (contents) => {
            for (const found of contents) {
                await updateFile(
                    resolved,
                    logger,
                    path.join(directory, found),
                    version
                );
                updatedPath = true;
            }
        });

        // If we didn't update a path, see if we can write this file.
        if (!updatedPath && !glob.hasMagic(pattern)) {
            updated = true;
            await createFile(
                resolved,
                logger,
                path.join(directory, pattern),
                version
            );
        }
    }

    // If we get all the way out and we haven't updated a path, we have a problem.
    if (!updated) {
        logger.warn("No non-globbed files were found to update .NET version");
    }
}

export async function prepare(
    options: PluginOptions,
    config: any
): Promise<void> {
    const { cwd, logger, nextRelease } = config;

    await update(options, logger, cwd, nextRelease.version);
}
