module.exports = {
    branches: ["main"],
    message: "chore(release): v${nextRelease.version}\n\n${nextRelease.notes}",
    plugins: [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@semantic-release/npm",
        "@semantic-release/changelog",
        "@semantic-release/git",
        "@semantic-release/gitlab",
    ],
};
